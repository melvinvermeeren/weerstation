/**
 * �2013 Melvin Vermeeren.
 * Application: Weerstation.
 * Initial release 30-Nov-2013.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 *
 * Made in Visual Studio 2013 with Visual Micro, CodeMaid and ReSharper.
 */


/* Configure modules, disabling modules reduces program footprint and increasing loops per second.
   If there is no valid input type enabled an animated error message will be looped. */
// Nothing here... yet?

/* Configure constants based off includes first. */
#include <limits.h> // Include limits.h since it provides constants of maximum value of variables. ULONG_MAX is used to prevent program freezing after ~50 days.
const unsigned long ULONG_MAX_MARGIN = ULONG_MAX - 50; // ULONG_MAX with margin removed to make sure that program won't freeze.

/* Variables for debugging. */
const bool debugCyclesEnable = false; // Display message every second saying how many times it went through loop().
unsigned long loopsPerSecond = 0; // Counter for amount of loops per second.
unsigned long secTimerLPS; // One-second timer for keeping track of loopsPerSecond properly.

/* Constants. */
const unsigned int LEDPINIDS[] = { 6, 7, 8, 9, 10, 11, 12, 13 }; // From left to right pin IDs.
const unsigned int LEDPINIDS_LENGTH = 8; // Length of LEDPINIDS.
const unsigned int LEDTEMPMIN = 15; // When this temperature is reached first LED turns on.
const unsigned int LEDTEMPMAX = 22; // When this temperature is reached final LED turns on.
const unsigned int BUTTONIDS[] = { 2, 3, 4, 5 }; // From left to right buttons. Changes wind directions to west, east, north, south in that order.
const unsigned int BUTTONIDS_LENGTH = 4; // Length of BUTTONIDS.
const int BUTTONIDS_REACTWHENSTATEIS[] = { HIGH, HIGH, HIGH, HIGH }; // React to button presses if the state is this, same order as BUTTONIDS[].
const unsigned int BLOCKBUTTONSFOR = 250; // Amount of milliseconds to ignore button input after a button is pressed.
const unsigned int POTMETERPINID = A2; // Potentiometer pin ID.
const unsigned int THERMOPINID = A1; // Thermistor pin ID.
const bool THERMOISNTC = true; // If true thermistor is NTC, else PTC.
const unsigned int LDRPINID = A0; // LDR pin ID.
const unsigned int READSENSORSEVERY = 50; // Update sensors every X milliseconds, this also controls how often information is sent to server.

/* Global variables. */
unsigned long buttonTimer = 0; // Timer for blocking button input.
unsigned long readSensorTimer = 0; // Timer for reading sensors.
float thermistorValue; // Latest thermistor reading in degrees Celsius.
unsigned int windDirection = 0; // 0 = west; 1 = east; 2 = north; 3 = south.


void setup()
{
	Serial.begin(9600); // Open serial port.

	for (unsigned int i = 0; i < LEDPINIDS_LENGTH; i++) pinMode(LEDPINIDS[i], OUTPUT); // Configure LEDs.
	for (unsigned int i = 0; i < BUTTONIDS_LENGTH; i++) pinMode(BUTTONIDS[i], INPUT); // Configure buttons.
	pinMode(POTMETERPINID, INPUT); // Configure potentiometer.
	pinMode(THERMOPINID, INPUT); // Configure thermistor.
	pinMode(LDRPINID, INPUT); // Configure LDR.
}


void loop()
{
	if (millis() > readSensorTimer) updateSensors();
	if (millis() > buttonTimer) processButtonInput();
}