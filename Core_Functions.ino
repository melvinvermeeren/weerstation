/**
* �2013 Melvin Vermeeren.
* This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
*/


/**
 * Update analogue sensors.
 */
void updateSensors()
{
	readSensorTimer = millis() + READSENSORSEVERY; // Reset timer.

	/* Potentiometer. */
	unsigned int potmeterValue = analogRead(POTMETERPINID);
	/* Thermistor. */
	if (THERMOISNTC) thermistorValue = thermistorToDegreesCelsius(analogRead(THERMOPINID));
	else thermistorValue = Serial.print(thermistorToDegreesCelsius(map(analogRead(THERMOPINID), 0, 1023, 1023, 0)));
	/* LDR. */
	unsigned int LDRValue = analogRead(LDRPINID);

	/* Print line and calculate checksum. */
	Serial.print(potmeterValue);
	Serial.print(";");

	Serial.print(thermistorValue);
	Serial.print(";");

	Serial.print(LDRValue);
	Serial.print(";");

	Serial.print(windDirection);
	Serial.print(";");

	Serial.print(calculateChecksum(potmeterValue, int(thermistorValue), LDRValue, windDirection));
	Serial.println(";");

	updateLEDs(); // Update LED states.
}


/**
* Convert sensor reading to degrees Celsius.
* @param sensor value.
* @return degrees Celsius.
*
* Note: print(...) automatically trims value down to 2 decimals.
* Calculations taken from: http://playground.arduino.cc/ComponentLib/Thermistor2.
* B-value (3977) taken from: http://iwantmyreal.name/blog/2012/09/23/measuring-the-temperature-with-an-arduino-and-a-thermistor/.
*/
float thermistorToDegreesCelsius(unsigned int sensorValue)
{
	double calcVar;
	calcVar = log(((10240000 / sensorValue) - 10000));
	calcVar = 1 / (0.001129148 + (0.00025144581342720643701282373648479 + (0.0000000876741 * calcVar * calcVar))* calcVar);
	return calcVar - 273.15; // Convert Kelvin to degrees Celsius.
}


/**
 * Calculate checksum based of 4 integers.
 * @param unsigned integer 1.
 * @param unsigned integer 2.
 * @param unsigned integer 3.
 * @param unsigned integer 4.
 * @return checksum.
 */
unsigned int calculateChecksum(unsigned int value1, unsigned int value2, unsigned int value3, unsigned int value4)
{
	return (value1 + value2 + value3 + value4) % 255;
}


/**
 * Update LEDs.
 */
void updateLEDs()
{
	unsigned int TurnOnLEDIDsBefore = map(thermistorValue, LEDTEMPMIN - 1, LEDTEMPMAX, 0, LEDPINIDS_LENGTH);

	for (unsigned int i = 0; i < LEDPINIDS_LENGTH; i++)
	{
		if (i < TurnOnLEDIDsBefore) digitalWrite(LEDPINIDS[i], HIGH);
		else digitalWrite(LEDPINIDS[i], LOW);
	}
}


/**
 * Process button input.
 */
void processButtonInput()
{
	buttonTimer = millis() + BLOCKBUTTONSFOR; // Reset timer.

	if (digitalRead(BUTTONIDS[0]) == BUTTONIDS_REACTWHENSTATEIS[0]) windDirection = 0;
	else if (digitalRead(BUTTONIDS[1]) == BUTTONIDS_REACTWHENSTATEIS[1]) windDirection = 1;
	else if (digitalRead(BUTTONIDS[2]) == BUTTONIDS_REACTWHENSTATEIS[2]) windDirection = 2;
	else if (digitalRead(BUTTONIDS[3]) == BUTTONIDS_REACTWHENSTATEIS[3]) windDirection = 3;
}