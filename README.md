Weerstation
===========

©2013 Melvin Vermeeren.  
Initial release 30-Nov-2013.  
This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.  
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.  
Made in Visual Studio 2013 with Visual Micro, CodeMaid and ReSharper.  

Button bindings:
----------------
* 	First button sets wind direction to west.
*	Second button sets wind direction to east.
*	Third button sets wind direction to north.
*	Fourth button setts wind direction to south.