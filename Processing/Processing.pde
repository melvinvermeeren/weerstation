/**
 * ©2013 Melvin Vermeeren.
 * Application: Weerstation.
 * Initial release 30-Nov-2013.
 *
 * This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/3.0/deed.en_US.
 *
 * Made in Processing 2.1 (x86).
 */


/* Import libraries. */
import processing.serial.*;
import processing.net.*;

/* Configure variables. */
Serial COMPORT;
int potmeter;
String thermistor;
String LDR;
String windDirection;
long lostConnectionTimer;
final int lostConnectionMessageAfter = 5000;

/* Configure webserver. */
final String HTTP_GET_REQUEST = "GET /";
final String HTTP_HEADER = "HTTP/1.0 200 OK\r\nContent-Type: text/html\r\n\r\n";
Server server;
Client client;
String serverInput;


void setup()
{
  size(500,300);
  COMPORT = new Serial(this, "COM4", 9600);
  COMPORT.bufferUntil('\n');
  server = new Server(this, 20000); // Start server at random port since I already have a lot of servers running on my PC.
  fill(255,0,0);
  stroke(0,255,0);
  textAlign(CENTER, CENTER);
  lostConnectionTimer = 0;
}


void draw()
{
  /* Reply to web requests. */
  try
  {
    client = server.available();
    if (client != null)
    {
      serverInput = client.readString();
      serverInput = serverInput.substring(0, serverInput.indexOf("\n"));
      
      if (serverInput.indexOf(HTTP_GET_REQUEST) == 0) // Client asking webpage, send it.
      {
        client.write(HTTP_HEADER);
        client.write("<!DOCTYPE html><html><head><meta charset=UTF-8 HTTP-EQUIV='refresh' CONTENT='1'><title>Weerstation</title><BODY BGCOLOR='000000' TEXT='FF0000'></head><body><h3>Weerstation.<br/><br/>Temperature: " + thermistor + " degrees Celsius.<br/>Light value (0 - 1023): " + LDR + ".<br/>Wind direction: " + windDirection + ".<br/>Wind speed (0 - 1023): " + potmeter + ".<br/><br/>The server has been running for: " + int(millis() / (1000 * 60 * 60 * 24)) + " day(s), " + int(millis() % (1000 * 60 * 60 * 24) / (1000 * 60 * 60)) + " hour(s), " + int(millis() % (1000 * 60 * 60) / (1000 * 60)) + " minute(s) and " + int(millis() % (1000 * 60) / 1000) + " second(s).</h3></body></html>");
        client.stop();
      }
    }
  }
  catch(Exception e) { } // This crashed before. So just keep on going if shit hits the fan and accepts the next HTTP request.
  
  /* Draw console locally. */
  if(millis() > lostConnectionTimer)
  {
    background(0, 0, 0);
    text("Error: Connection lost.", width/2, height/2 - 20);
    text("Verify that the Arduino is properly connected", width/2, height/2);
    text("and restart the application.", width/2, height/2 + 20);
  }
  else
  {
    background(0, 0, 0);
    text("Temperature: " + thermistor + " degrees Celsius.", width/2, height/2 - 40);
    text("Light value (0 - 1023): " + LDR + ".", width/2, height/2 - 20);
    text("Wind direction: " + windDirection + ".", width/2, height/2);
    text("Wind speed (0 - 1023): " + potmeter + ".", width/2, height/2 + 20);
    text("The server has been running for: " + int(millis() / (1000 * 60 * 60 * 24)) + " day(s), " + int(millis() % (1000 * 60 * 60 * 24) / (1000 * 60 * 60)) + " hour(s), " + int(millis() % (1000 * 60 * 60) / (1000 * 60)) + " minute(s) and " + int(millis() % (1000 * 60) / 1000) + " second(s).", width/2, height/2 + 40);
  }
}


void serialEvent(Serial COMPORT)
{
  lostConnectionTimer = millis() + lostConnectionMessageAfter; // Reset timer.
  
  String inputString[] = split((new String(COMPORT.readBytesUntil('\n'))), ";"); // Split the message in actual values.
  
  /* Calculate checksum. */
  int checksum = (int(inputString[0]) + int(inputString[1]) + int(inputString[2]) + int(inputString[3])) % 255; // Calculate checksum.
  int checksumShouldBe = int(inputString[4]); // Extract checksum from message.
  
  /* Only update variables if checksum is valid. */
  if (checksum == checksumShouldBe)
  {
    /* Set new values for potmeter, thermistor and LDR. */
    potmeter = int(inputString[0]);
    thermistor = inputString[1];
    LDR = inputString[2];
    
    /* Set wind direction. */
    switch(int(inputString[3]))
    {
    case 0:
      windDirection = "West";
      break;
    
    case 1:
      windDirection = "East";
      break;
    
    case 2:
      windDirection = "North";
      break;
    
    case 3:
      windDirection = "South";
      break;
    
    default:
      println("Invalid wind direction input! Checksum failed? Bugs in arduino code?");
      break;
    }
  }
  else println("Invalid checksum, input ignored.");
}
